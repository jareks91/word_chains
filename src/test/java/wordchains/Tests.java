package wordchains;

import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Tests {
  public static List<String> list(String... values) {
    return Arrays.asList(values);
  }

  public static Set<String> set(String... values) {
    return Sets.newHashSet(values);
  }
}
