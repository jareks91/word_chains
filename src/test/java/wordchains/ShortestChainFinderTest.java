package wordchains;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static wordchains.Tests.list;
import static wordchains.Tests.set;
import wordchains.dictionary.Dictionary;
import wordchains.exceptions.ChainNotFoundException;

public class ShortestChainFinderTest {
  @Test
  public void threeWordsInChain() throws Exception {
    Dictionary dictionary = createDictionary();
    when(dictionary.getWordsWithOneCharDiff("cat")).thenReturn(set("caX", "Xat"));
    when(dictionary.getWordsWithOneCharDiff("caX")).thenReturn(set("car", "caX"));

    WordChainsGame game = new WordChainsGame(wordLength -> dictionary);
    assertEquals(
        list("cat", "caX", "car"),
        game.play("cat", "car"));
  }

  @Test
  public void twoExistingChains() throws Exception {
    Dictionary dictionary = createDictionary();
    addTwoChains(dictionary);
    WordChainsGame game = new WordChainsGame(wordLength -> dictionary);

    assertEquals(
        list("lead", "load", "goad", "gold"),
        game.play("lead", "gold"));
  }

  @Test(expected = ChainNotFoundException.class)
  public void cannotFindChain() throws Exception {
    Dictionary dictionary = createDictionary();
    addTwoChains(dictionary);
    WordChainsGame game = new WordChainsGame(wordLength -> dictionary);

    assertTrue(game.play("cat", "dog").isEmpty());
  }

  private void addTwoChains(Dictionary dictionary) {
    when(dictionary.getWordsWithOneCharDiff("lead")).thenReturn(set("lXad", "load", "leXd"));
    when(dictionary.getWordsWithOneCharDiff("leXd")).thenReturn(set("loXd", "lXXd"));
    when(dictionary.getWordsWithOneCharDiff("loXd")).thenReturn(set("goXd"));
    when(dictionary.getWordsWithOneCharDiff("goXd")).thenReturn(set("gold"));
    when(dictionary.getWordsWithOneCharDiff("load")).thenReturn(set("lXad", "goad"));
    when(dictionary.getWordsWithOneCharDiff("lXad")).thenReturn(set("lXaX"));
    when(dictionary.getWordsWithOneCharDiff("goad")).thenReturn(set("gold"));
  }

  private Dictionary createDictionary() {
    Dictionary dictionary = mock(Dictionary.class);
    when(dictionary.isInDictionary(any())).thenReturn(true);
    return dictionary;
  }

}