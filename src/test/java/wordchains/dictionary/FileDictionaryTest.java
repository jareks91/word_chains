package wordchains.dictionary;

import org.junit.Test;

import static java.util.Collections.emptySet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static wordchains.Tests.set;

public class FileDictionaryTest {

  @Test
  public void isInDictionary() throws Exception {
    assertTrue(new FileDictionary("wordList.txt", 1).isInDictionary("A"));
    assertFalse(new FileDictionary("wordList.txt", 5).isInDictionary("ABD21"));
  }

  @Test
  public void canReturnWordsWithOneCharDiff() throws Exception {
    FileDictionary dictionary = new FileDictionary("testWordList.txt", 4);
    assertEquals(
        set("goXd", "golX", "Xold"),
        dictionary.getWordsWithOneCharDiff("gold")
    );

    assertEquals(
        emptySet(),
        dictionary.getWordsWithOneCharDiff("note")
    );
  }


  @Test
  public void oneCharDiff() throws Exception {
    FileDictionary dictionary = new FileDictionary("testWordList.txt" ,4);
    assertTrue(dictionary.isOneCharDiff("load", "Xoad"));
    assertTrue(dictionary.isOneCharDiff("load", "lXad"));
    assertTrue(dictionary.isOneCharDiff("load", "loXd"));
    assertTrue(dictionary.isOneCharDiff("load", "loaX"));

    assertFalse(dictionary.isOneCharDiff("load", "cat"));
    assertFalse(dictionary.isOneCharDiff("load", "XXad"));
    assertFalse(dictionary.isOneCharDiff("load", "lXXd"));
    assertFalse(dictionary.isOneCharDiff("load", "loXX"));
    assertFalse(dictionary.isOneCharDiff("load", "XXXX"));
    assertFalse(dictionary.isOneCharDiff("load", "load"));
  }
}
