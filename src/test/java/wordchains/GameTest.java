package wordchains;

import com.google.common.base.Stopwatch;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static wordchains.Tests.list;
import wordchains.dictionary.FileDictionaryFactory;

public class GameTest {
  @Test
  public void catDog() throws Exception {
    WordChainsGame game = new WordChainsGame(new FileDictionaryFactory("wordList.txt"));

    assertEquals(
        list("cat", "cot", "dot", "dog"),
        game.play("cat", "dog"));
  }

  @Test
  public void leadGold() throws Exception {
    WordChainsGame game = new WordChainsGame(new FileDictionaryFactory("wordList.txt"));

    assertEquals(
        list("lead", "load", "goad", "gold"),
        game.play("lead", "gold"));
  }

  @Test
  public void rubyCode() throws Exception {
    WordChainsGame game = new WordChainsGame(new FileDictionaryFactory("wordList.txt"));

    assertEquals(
        list("ruby", "rube", "rude", "rode", "code"),
        game.play("ruby", "code"));
  }

  @Test
  public void houseClone() throws Exception {
    WordChainsGame game = new WordChainsGame(new FileDictionaryFactory("wordList.txt"));

    System.out.println(
        game.play("house", "clone")
    );
  }

  @Test
  public void resultShouldBeReturnWithinSecond() throws Exception {
    WordChainsGame game = new WordChainsGame(new FileDictionaryFactory("wordList.txt"));

    Stopwatch st = Stopwatch.createStarted();
    assertEquals(
        list("lead", "load", "goad", "gold"),
        game.play("lead", "gold"));
    assertTrue(st.stop().elapsed(TimeUnit.MILLISECONDS) < 1000);

    st = Stopwatch.createStarted();
    assertEquals(
        list("gold", "goad", "load", "lead"),
        game.play("gold", "lead")
    );
    assertTrue(st.stop().elapsed(TimeUnit.MILLISECONDS) < 1000);
  }
}
