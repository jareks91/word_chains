package wordchains;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static wordchains.Tests.list;
import wordchains.dictionary.Dictionary;

public class WordChainsTest {

  @Test
  public void invalidInputs() throws Exception {
    WordChainsGame game = new WordChainsGame(wordLength -> createDictionary());
    try {
      game.play("car", "house");
      fail();
    } catch (Exception e) {
      assertEquals("Words have a different length", e.getMessage());
    }
  }

  @Test
  public void startWordTheSameAsEndWord() throws Exception {
    WordChainsGame game = new WordChainsGame(wordLength -> createDictionary());
    assertEquals(
        list("cat", "cat"),
        game.play("cat", "cat"));
  }

  @Test
  public void wordsNeedsToBeInDictionary() throws Exception {
    Dictionary dictionary = mock(Dictionary.class);
    when(dictionary.isInDictionary("notInDictionary")).thenReturn(false);
    when(dictionary.isInDictionary("XXXInDictionary")).thenReturn(true);

    WordChainsGame game = new WordChainsGame(wordLength -> dictionary);

    try {
      game.play("notInDictionary", "XXXInDictionary");
      fail();
    } catch (Exception exception) {
      assertEquals("Word [notInDictionary] is not in the dictionary", exception.getMessage());
    }

    try {
      game.play("XXXInDictionary", "notInDictionary");
      fail();
    } catch (Exception exception) {
      assertEquals("Word [notInDictionary] is not in the dictionary", exception.getMessage());
    }
  }

  private Dictionary createDictionary() {
    Dictionary dictionary = mock(Dictionary.class);
    when(dictionary.isInDictionary(any())).thenReturn(true);
    return dictionary;
  }
}
