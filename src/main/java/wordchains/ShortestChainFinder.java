package wordchains;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import wordchains.dictionary.Dictionary;
import wordchains.exceptions.ChainNotFoundException;

class ShortestChainFinder {
  private final Dictionary dictionary;

  public ShortestChainFinder(Dictionary dictionary) {
    this.dictionary = dictionary;
  }

  public Node find(String startWord, String endWord) throws ChainNotFoundException {
    Set<String> checkedWord = new HashSet<>();
    List<Node> currentLevelNodes = createInitialCurrentLevelNode(startWord);

    while (!currentLevelNodes.isEmpty()) {
      List<Node> nextLevelNodes = new ArrayList<>();

      for (Node currentNode : currentLevelNodes) {
        Set<String> newWords = dictionary.getWordsWithOneCharDiff(currentNode.getValue());

        if (newWords.contains(endWord))
          return new Node(endWord, currentNode.getChain());

        nextLevelNodes.addAll(createNextLevelNodes(newWords, checkedWord, currentNode));
      }

      currentLevelNodes.clear();
      currentLevelNodes.addAll(nextLevelNodes);
    }
    throw new ChainNotFoundException();
  }

  private List<Node> createInitialCurrentLevelNode(String startWord) {
    List<Node> out = new ArrayList<>();
    out.add(new Node(startWord));
    return out;
  }

  private List<Node> createNextLevelNodes(Collection<String> newWords, Set<String> checkedWord, Node currentNode) {
    return newWords.stream()
        .filter(checkedWord::add)
        .map(word -> new Node(word, currentNode.getChain()))
        .collect(Collectors.toList());
  }
}
