package wordchains;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Node {
  private final String value;
  private final List<String> chain;

  Node(String value, List<String> chain) {
    this.chain = new ArrayList<>(chain);
    this.chain.add(value);
    this.value = value;
  }

  Node(String value) {
    this(value, Collections.emptyList());
  }

  String getValue() {
    return value;
  }

  List<String> getChain() {
    return chain;
  }
}
