package wordchains;

import com.google.common.collect.Lists;

import java.util.List;

import wordchains.dictionary.Dictionary;
import wordchains.dictionary.DictionaryFactory;
import wordchains.exceptions.ChainNotFoundException;
import wordchains.exceptions.InvalidInputException;

class WordChainsGame {
  private final DictionaryFactory dictionaryFactory;

  WordChainsGame(DictionaryFactory dictionaryFactory) {
    this.dictionaryFactory = dictionaryFactory;
  }

  List<String> play(String startWord, String endWord) throws ChainNotFoundException, InvalidInputException {
    Dictionary dictionary = dictionaryFactory.create(startWord.length());

    checkInputs(startWord, endWord, dictionary);

    if (startWord.equals(endWord))
      return Lists.newArrayList(startWord, endWord);

    return new ShortestChainFinder(dictionary).find(startWord, endWord)
        .getChain();
  }

  private void checkInputs(String startWord, String endWord, Dictionary dictionary) throws InvalidInputException {
    if (startWord.length() != endWord.length())
      throw new InvalidInputException("Words have a different length");

    checkWordInDictionary(startWord, dictionary);
    checkWordInDictionary(endWord, dictionary);
  }

  private void checkWordInDictionary(String startWord, Dictionary dictionary) throws InvalidInputException {
    if (!dictionary.isInDictionary(startWord))
      throw new InvalidInputException(String.format("Word [%s] is not in the dictionary", startWord));
  }
}

