package wordchains.dictionary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

public class FileDictionaryFactory implements DictionaryFactory {
  private final String path;

  private final static Logger logger = LoggerFactory.getLogger(FileDictionary.class);

  public FileDictionaryFactory(String path) {
    this.path = path;
  }

  @Override
  public Dictionary create(int wordLength) {
    try {
      return new FileDictionary(path, wordLength);
    } catch (IOException e) {
      logger.error("Unable to create FileDictionary", e);
      return new EmptyDictionary();
    }
  }

  private class EmptyDictionary implements Dictionary {
    @Override
    public Set<String> getWordsWithOneCharDiff(String word) {
      return Collections.emptySet();
    }

    @Override
    public boolean isInDictionary(String word) {
      return false;
    }
  }
}
