package wordchains.dictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.stream.Collectors;

public class FileDictionary implements Dictionary {
  private Set<String> words;

  public FileDictionary(String path, int wordLength) throws IOException {
    Resources.getResourcesAsStream(path, is -> loadDictionary(is, wordLength));
  }

  private void loadDictionary(InputStream inputStream, int wordLength) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
    this.words = reader.lines()
        .filter(s -> s.length() == wordLength)
        .collect(Collectors.toSet());
  }

  @Override
  public Set<String> getWordsWithOneCharDiff(String word) {
    return words.stream()
        .filter(w -> isOneCharDiff(w, word))
        .collect(Collectors.toSet());
  }

  boolean isOneCharDiff(String w1, String w2) {
    char[] w1Chars = w1.toCharArray();
    char[] w2Chars = w2.toCharArray();

    int diff = 0;
    for (int charIndex = 0; charIndex < w1.length(); charIndex++) {
      if (w1Chars[charIndex] != w2Chars[charIndex]) {
        diff++;
        if (diff > 1)
          return false;
      }
    }

    return diff != 0;
  }

  @Override
  public boolean isInDictionary(String word) {
    return words.contains(word);
  }
}
