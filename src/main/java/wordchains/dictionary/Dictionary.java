package wordchains.dictionary;

import java.util.Set;

public interface Dictionary {

  Set<String> getWordsWithOneCharDiff(String word);

  boolean isInDictionary(String word);
}
