package wordchains.dictionary;

public interface DictionaryFactory {
  Dictionary create(int wordLength);
}
