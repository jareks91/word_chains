package wordchains.dictionary;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

class Resources {
  public static void getResourcesAsStream(String path, Consumer<InputStream> consumer) throws IOException {
    try (InputStream stream = Resources.class.getClassLoader().getResourceAsStream(path)) {
      consumer.accept(stream);
    }
  }
}
